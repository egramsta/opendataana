from ROOT import *
import sys, os, time

t0 = time.time() 

arg1 = sys.argv[1]  

if arg1 == 'Data': 
        input_dir = '/scratch3/eirikgr/openData_13TeV/Data/'
elif arg1 == 'Fakes': 
        input_dir = '/scratch3/eirikgr/openData_13TeV/Data/'
elif arg1 == 'MC': 
        input_dir = '/scratch3/eirikgr/openData_13TeV/MC/BSM_Signal_Samples/'
        input_dir1 = '/scratch3/eirikgr/openData_13TeV/MC/SM_Backgrounds/'


myChain = TChain('mini') 


for filename in os.listdir(input_dir):
	#if not "dataA_2lep.root" in filename: continue
        if not '.root' in filename: continue 
        print filename  
        myChain.Add(input_dir+filename) 
if arg1 == 'MC' and input_dir1: 
        for filename in os.listdir(input_dir1):
		#if not 'mc15_13TeV.364114.Sh_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.2lep_raw.root' in filename: continue 
                if not '.root' in filename: continue 
                print filename  
                myChain.Add(input_dir1+filename) 


entries = myChain.GetEntries() 

print "-------------------------------------------"
if arg1 == 'Data': 
        print "Running on real data!"
elif arg1 == 'Fakes': 
        print "Running on fakes!"
else: 
        print "Running on Monte Carlo!" 
print "Number of events to process: %d" %entries
print "-------------------------------------------"

gROOT.ProcessLine(".L MatrixMethod.cxx+")
#if arg1 == 'Data': 
myChain.Process("MySelector.C++", arg1)
#else: 
#        myChain.Process("MySelector.C++", "MC") 

t = int( time.time()-t0 )/60  

print "Time spent: %d min" %t 
