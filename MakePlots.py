import ROOT
from ROOT import *
import os, sys
import array
from math import *
from infofile import infos

colours = dict(Diboson=kAzure+1, Top=kRed+1, Zjets=kOrange-2, Wjets=kGray, Higgs=kPink) 

def doRebin(hT,bins=array.array('f',[0,10,20,25,30,40,50,60,80,100,200]),isData = True, bkg = ""):
    VB = 0
    name = ""
    name = hT.GetName()+"_REBIN"
    print hT.GetName()
    hT_reb = TH1F(name,name,len(bins)-1,bins)
    if isData:
        hT_reb.SetMarkerStyle(20)
        hT_reb.SetMarkerSize(0.7)
        hT_reb.SetLineColor(kBlack)
    else:
	hT_reb.SetFillColor(colours[bkg])
	hT_reb.SetLineColor(colours[bkg])
        
                
    #hT_reb.SetDirectory(0)
    #hT_reb.ResetStats()
    for nbin in range(1,hT.GetNbinsX()+2):
        if VB > 1: print("Adding %.2f from value %.2f into bin %i" %(hT.GetBinContent(nbin),abs(hT.GetBinLowEdge(nbin)),hT_reb.FindBin(abs(hT.GetBinLowEdge(nbin)))))
        hT_reb.AddBinContent(hT_reb.FindBin(abs(hT.GetBinLowEdge(nbin))),hT.GetBinContent(nbin))
    for nbin in range(1,hT_reb.GetNbinsX()+2):
        hT_reb.SetBinError(nbin,sqrt(hT_reb.GetBinContent(nbin)) if hT_reb.GetBinContent(nbin) >= 0 else 0.0)
    return hT_reb


def setRatioErrors(hT,hL,hE):
        asym = TGraphAsymmErrors();
        asym.Divide(hT,hL,"cl=0.683 b(1,1) mode")
        for i in range(0,hE.GetNbinsX()-1):
            #print "Error in bin %i  is %.2f" %(i+1,asym.GetErrorY(i))
            hE.SetBinError(i+1,asym.GetErrorY(i))
                

def makeBeamerSlides(madeFiles):
    str = "\\makeatletter\\let\\ifGm@compatii\\relax\\makeatother\n"
    str += "\\documentclass[9pt]{beamer}\n"
    str += "\\usepackage[english]{babel}\n"
    str += "\\usepackage[latin1]{inputenc}\n"
    str += "\\usepackage[T1]{fontenc}\n"
    str += "\\usepackage{multicol,color,array,mathrsfs,multirow,graphicx}\n\n"
    str += "\\begin{document}\n"
    
    nf = 0
    totf = len(madeFiles)
    for f in madeFiles:
        if nf%2 == 0:
            #print nf, f
            if nf > 0:
		str += "\\end{columns}\n"
		str += "\\end{frame}\n\n\n"
	    str += "\\begin{frame}[fragile]\n"
	    str += "\\begin{columns}\n"
        str += "\\column{%.1f\\textwidth}\n" %(1.0 if nf == (totf-1) else 0.5)
	str += "\\includegraphics[width=\\textwidth]{%s}\n" %f
	nf += 1
        # if nf%2 == 0:
    str += "\\end{columns}\n"
    str += "\\end{frame}\n\n\n"
    str += "\\end{document}\n"
    return str;
		


def getSigDic():
	sigdic = {}
	sigdic["301215"] = "Py8EG_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi2000"
	sigdic["301216"] = "Py8EG_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi3000"
	sigdic["301217"] = "Py8EG_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi4000"
	sigdic["301218"] = "Py8EG_A14NNPDF23LO_Zprime_NoInt_ee_E6Chi5000"
	sigdic["301220"] = "Py8EG_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi2000"
	sigdic["301221"] = "Py8EG_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi3000"
	sigdic["301222"] = "Py8EG_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi4000"
	sigdic["301223"] = "Py8EG_A14NNPDF23LO_Zprime_NoInt_mumu_E6Chi5000"
	sigdic["301322"] = "Py8EG_A14NNPDF23LO_zprime400_tt"
	sigdic["301323"] = "Py8EG_A14NNPDF23LO_zprime500_tt"
	sigdic["301324"] = "Py8EG_A14NNPDF23LO_zprime750_tt"
	sigdic["301325"] = "Py8EG_A14NNPDF23LO_zprime1000_tt"
	sigdic["301326"] = "Py8EG_A14NNPDF23LO_zprime1250_tt"
	sigdic["301327"] = "Py8EG_A14NNPDF23LO_zprime1500_tt"
	sigdic["301328"] = "Py8EG_A14NNPDF23LO_zprime1750_tt"
	sigdic["301329"] = "Py8EG_A14NNPDF23LO_zprime2000_tt"
	sigdic["301330"] = "Py8EG_A14NNPDF23LO_zprime2250_tt"
	sigdic["301331"] = "Py8EG_A14NNPDF23LO_zprime2500_tt"
	sigdic["301332"] = "Py8EG_A14NNPDF23LO_zprime2750_tt"
	sigdic["301333"] = "Py8EG_A14NNPDF23LO_zprime3000_tt"
	sigdic["303329"] = "MGPy8EG_A14NNPDF23LO_RS_G_ZZ_llll_c10_m1000"
	sigdic["303334"] = "MGPy8EG_A14NNPDF23LO_RS_G_ZZ_llll_c10_m2000"
	sigdic["303511"] = "MGPy8EG_A14NNPDF30_dmV_Zll_MET40_DM1_MM10"
	sigdic["303512"] = "MGPy8EG_A14NNPDF30_dmV_Zll_MET40_DM1_MM100"
	sigdic["303513"] = "MGPy8EG_A14NNPDF30_dmV_Zll_MET40_DM1_MM300"
	sigdic["303514"] = "MGPy8EG_A14NNPDF30_dmV_Zll_MET40_DM1_MM200"
	sigdic["305550"] = "Py8EG_A14NNPDF23LO_Gee_01_750"
	sigdic["305553"] = "Py8EG_A14NNPDF23LO_Gee_01_1000"
	sigdic["305556"] = "Py8EG_A14NNPDF23LO_Gee_01_2000"
	sigdic["305559"] = "Py8EG_A14NNPDF23LO_Gee_01_3000"
	sigdic["305562"] = "Py8EG_A14NNPDF23LO_Gee_01_4000"
	sigdic["305568"] = "Py8EG_A14NNPDF23LO_Gmumu_01_750"
	sigdic["305571"] = "Py8EG_A14NNPDF23LO_Gmumu_01_1000"
	sigdic["305574"] = "Py8EG_A14NNPDF23LO_Gmumu_01_2000"
	sigdic["305577"] = "Py8EG_A14NNPDF23LO_Gmumu_01_3000"
	sigdic["305710"] = "MGPy8EG_A14NNPDF30_dmV_Zll_MET40_DM1_MM500"
	sigdic["305711"] = "MGPy8EG_A14NNPDF30_dmV_Zll_MET40_DM1_MM700"
	sigdic["306085"] = "MGPy8EG_A14NNPDF30_dmV_Zll_MET40_DM1_MM200"
	sigdic["306093"] = "MGPy8EG_A14NNPDF30_dmV_Zll_MET40_DM1_MM400"
	sigdic["306103"] = "MGPy8EG_A14NNPDF30_dmV_Zll_MET40_DM1_MM600"
	sigdic["306109"] = "MGPy8EG_A14NNPDF30_dmV_Zll_MET40_DM1_MM800"
	sigdic["307431"] = "MGPy8EG_A14NNPDF23LO_RS_G_ZZ_llll_c10_m0200"
	sigdic["307434"] = "MGPy8EG_A14NNPDF23LO_RS_G_ZZ_llll_c10_m0500"
	sigdic["307439"] = "MGPy8EG_A14NNPDF23LO_RS_G_ZZ_llll_c10_m1500"
	sigdic["370114"] = "MGPy8EG_A14N_GG_ttn1_1200_5000_1"
	sigdic["370118"] = "MGPy8EG_A14N_GG_ttn1_1200_5000_600"
	sigdic["370129"] = "MGPy8EG_A14N_GG_ttn1_1400_5000_1"
	sigdic["370144"] = "MGPy8EG_A14N_GG_ttn1_1600_5000_1"
	sigdic["387154"] = "MGPy8EG_A14NNPDF23LO_TT_directTT_500_1"
	sigdic["387157"] = "MGPy8EG_A14NNPDF23LO_TT_directTT_500_200"
	sigdic["387163"] = "MGPy8EG_A14NNPDF23LO_TT_directTT_600_1"
	sigdic["388240"] = "MGPy8EG_A14NNPDF23LO_TT_directTT_450_1"
	sigdic["392217"] = "MGPy8EG_A14N23LO_C1N2_WZ_400p0_0p0_3L_2L7"
	sigdic["392220"] = "MGPy8EG_A14N23LO_C1N2_WZ_350p0_0p0_3L_2L7"
	sigdic["392223"] = "MGPy8EG_A14N23LO_C1N2_WZ_500p0_0p0_3L_2L7"
	sigdic["392226"] = "MGPy8EG_A14N23LO_C1N2_WZ_100p0_0p0_3L_2L7"
	sigdic["392302"] = "MGPy8EG_A14N23LO_C1N2_WZ_500p0_100p0_2L2J_2L7"
	sigdic["392304"] = "MGPy8EG_A14N23LO_C1N2_WZ_300p0_100p0_2L2J_2L7"
	sigdic["392308"] = "MGPy8EG_A14N23LO_C1N2_WZ_300p0_200p0_2L2J_2L7"
	sigdic["392317"] = "MGPy8EG_A14N23LO_C1N2_WZ_400p0_0p0_2L2J_2L7"
	sigdic["392323"] = "MGPy8EG_A14N23LO_C1N2_WZ_500p0_0p0_2L2J_2L7"
	sigdic["392324"] = "MGPy8EG_A14N23LO_C1N2_WZ_400p0_300p0_2L2J_2L7"
	sigdic["392326"] = "MGPy8EG_A14N23LO_C1N2_WZ_100p0_0p0_2L2J_2L7"
	sigdic["392330"] = "MGPy8EG_A14N23LO_C1N2_WZ_200p0_100p0_2L2J_2L7"
	sigdic["392332"] = "MGPy8EG_A14N23LO_C1N2_WZ_500p0_300p0_2L2J_2L7"
	sigdic["392354"] = "MGPy8EG_A14N23LO_C1N2_WZ_600_100_2L2J_2L7"
	sigdic["392356"] = "MGPy8EG_A14N23LO_C1N2_WZ_600_0_2L2J_2L7"
	sigdic["392361"] = "MGPy8EG_A14N23LO_C1N2_WZ_700_400_2L2J_2L7"
	sigdic["392364"] = "MGPy8EG_A14N23LO_C1N2_WZ_700_100_2L2J_2L7"
	sigdic["392365"] = "MGPy8EG_A14N23LO_C1N2_WZ_700_0_2L2J_2L7"
	sigdic["392501"] = "MGPy8EG_A14N23LO_C1C1_SlepSnu_x0p50_200p0_100p0_2L8"
	sigdic["392502"] = "MGPy8EG_A14N23LO_C1C1_SlepSnu_x0p50_200p0_150p0_2L8"
	sigdic["392504"] = "MGPy8EG_A14N23LO_C1C1_SlepSnu_x0p50_300p0_100p0_2L8"
	sigdic["392506"] = "MGPy8EG_A14N23LO_C1C1_SlepSnu_x0p50_300p0_250p0_2L8"
	sigdic["392507"] = "MGPy8EG_A14N23LO_C1C1_SlepSnu_x0p50_400p0_100p0_2L8"
	sigdic["392509"] = "MGPy8EG_A14N23LO_C1C1_SlepSnu_x0p50_400p0_300p0_2L8"
	sigdic["392513"] = "MGPy8EG_A14N23LO_C1C1_SlepSnu_x0p50_500p0_300p0_2L8"
	sigdic["392517"] = "MGPy8EG_A14N23LO_C1C1_SlepSnu_x0p50_600p0_300p0_2L8"
	sigdic["392518"] = "MGPy8EG_A14N23LO_C1C1_SlepSnu_x0p50_700p0_1p0_2L8"  
	sigdic["392521"] = "MGPy8EG_A14N23LO_C1C1_SlepSnu_x0p50_700p0_300p0_2L8"
	sigdic["392916"] = "MGPy8EG_A14N23LO_SlepSlep_direct_100p5_1p0_2L8"
	sigdic["392918"] = "MGPy8EG_A14N23LO_SlepSlep_direct_200p5_1p0_2L8"
	sigdic["392920"] = "MGPy8EG_A14N23LO_SlepSlep_direct_300p5_1p0_2L8"
	sigdic["392924"] = "MGPy8EG_A14N23LO_SlepSlep_direct_500p5_1p0_2L8"
	sigdic["392925"] = "MGPy8EG_A14N23LO_SlepSlep_direct_100p0_50p0_2L8"
	sigdic["392936"] = "MGPy8EG_A14N23LO_SlepSlep_direct_200p0_100p0_2L8"
	sigdic["392942"] = "MGPy8EG_A14N23LO_SlepSlep_direct_500p0_100p0_2L8"
	sigdic["392951"] = "MGPy8EG_A14N23LO_SlepSlep_direct_300p0_200p0_2L8"
	sigdic["392962"] = "MGPy8EG_A14N23LO_SlepSlep_direct_400p0_300p0_2L8"
	sigdic["392964"] = "MGPy8EG_A14N23LO_SlepSlep_direct_500p0_300p0_2L8"
	sigdic["392982"] = "MGPy8EG_A14N23LO_SlepSlep_direct_600p0_1p0_2L8"
	sigdic["392985"] = "MGPy8EG_A14N23LO_SlepSlep_direct_600p0_300p0_2L8"
	sigdic["392996"] = "MGPy8EG_A14N23LO_SlepSlep_direct_700p0_1p0_2L8"
	sigdic["392999"] = "MGPy8EG_A14N23LO_SlepSlep_direct_700p0_300p0_2L8"
	sigdic["302681"] = "MGPy8EG_A14NNPDF23LO_LRSM_WR2400_NR1800"
	sigdic["302687"] = "MGPy8EG_A14NNPDF23LO_LRSM_WR3000_NR1500"
	sigdic["302701"] = "MGPy8EG_A14NNPDF23LO_LRSM_WR3600_NR1800"
	sigdic["302708"] = "MGPy8EG_A14NNPDF23LO_LRSM_WR4200_NR2100"
	sigdic["309070"] = "MGPy8EG_A14NNPDF23LO_LRSM_WR5000_NR2500"

	return sigdic


ROOT.gROOT.SetBatch(1)
ROOT.gStyle.SetOptStat(0);
ROOT.gStyle.SetPadLeftMargin(0.13)
ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gStyle.SetPalette(1)
ROOT.gStyle.SetGridStyle(2)
ROOT.gStyle.SetPadLeftMargin(0.13)
ROOT.TH1.AddDirectory(kFALSE)

channel = sys.argv[1].split(",") 

plotvar = sys.argv[2] 

indir = "Histograms"#"Histograms"


# Variables 

variables = ['pt1', 'pt2', 'eta1', 'eta2', 'phi1', 'phi2', 'mll', 'met', 'z0', 'z0sintheta', 'd0sigd0','etcone20','ptxetcone20','nlepsignal','nlepbaseline','trigDec','dRlepljet','dRlepbjet','jetpt','jeteta','jetMV2c10','nljet','njet','nbjet','mettev','jetjvt','mt2','pt']


isuu = False
isee = False
issf = False
isdf = False
isfake = False
for ch in channel:
    if "uu" in ch: isuu = True
    if "ee" in ch: isee = True
    if "sf" in ch: issf = True
    if "eu" in ch: isdf = True
    if "_T" in ch or "_L" in ch: isfake = True

for ch in channel:
    if "signal" in ch:
        variables += ['mll_100_150',
            'mll_150_200',
            'mll_200_300',
            'mll_300_inf',          
            'mll_100_inf',
            'mll_130_inf',
            'mll_150_inf',
            'mll_200_inf']
        break


# for ch in channel:
#         if "CR" in ch:
# 	        variables.append("pt")
#                 break
        
xtitles = {'pt1':'Leading lepton p_{T} (GeV)', 'pt2':'Subleading lepton p_{T} (GeV)', 'eta1':'Leading lepton #eta', 'eta2':'Subleading lepton #eta', 'phi1':'Leading lepton #phi', 'phi2':'Subleading lepton #phi', 'mll':'m_{ll} (GeV)', 'met':'E_{T}^{miss} (GeV)','z0':'z_{0} [mm]','z0sintheta':'|z_{0}sin#theta|','d0sigd0':'|d_{0}|/#sigma(d_{0})','etcone20':'etcone20 [GeV]','ptxetcone20':'etcone20/p_{T}','nlepsignal':'Number of signal leptons','nlepbaseline':'Number of baseline leptons','trigDec':'Trigger Descision','dRlepljet':'#DeltaR(lep,light-jet)','dRlepbjet':'#DeltaR(lep,b-jet)','jetpt':'p_{T} of jets','jeteta':'#eta of jets','jetMV2c10':'jetMV2c10','nljet':'N(light jets)','njet':'N(all jets)','nbjet':'N(b-jets)','mettev':'E_{T}^{miss} (TeV)','mt2':'m_{T2}','jetjvt':'Jet Vertex Tagger','pt':'lepton p_{T} (GeV)', 
           'mll_100_150':'m_{ll}',
           'mll_150_200':'m_{ll}',
           'mll_200_300':'m_{ll}',
           'mll_300_inf':'m_{ll}',          
           'mll_100_inf':'m_{ll}',
           'mll_130_inf':'m_{ll}',
           'mll_150_inf':'m_{ll}',
           'mll_200_inf':'m_{ll}'}

rebin = {'mt2':10,'pt1':4,'pt2':4,'mll':10,'etcone20':2,'met':10,'z0':2,'z0sintheta':5,'d0sigd0':2, 'ptxetcone20':5,'jetpt':5,'jetjvt':5,'dRlepbjet':2,'dRlepljet':2, 'pt':1 if isfake else 25, 
         'mll_100_150':5,
         'mll_150_200':5,
         'mll_200_300':5,
         'mll_300_inf':5,          
         'mll_100_inf':5,
         'mll_130_inf':5,
         'mll_150_inf':5,
         'mll_200_inf':5}

xmax = {'pt1':1000,'pt2':800,'mt2':750,'ptxetcone20':0.0,'mll':1600,'etcone20':60,'met':1000, 'd0sigd0':10, 'nbjet':10,'pt':1000,'dRlepljet':7,'dRlepbjet':7,
        'mll_100_150':1000,
         'mll_150_200':1000,
         'mll_200_300':1000,
         'mll_300_inf':1000,          
         'mll_100_inf':500,
         'mll_130_inf':1000,
         'mll_150_inf':1000,
         'mll_200_inf':1000}#,'z0sintheta':150}#,'pt':100}
xmin = {'pt1':0,'pt2':0,'mt2':0,'ptxetcone20':1.0,'mll':0,'etcone20':-20,'met':0, 'd0sigd0':0, 'nbjet':0,'pt':0,'dRlepbjet':0,'dRlepljet':0,'pt':0,'z0sintheta':0, 'mll':0,
        'mll_100_150':0,
         'mll_150_200':0,
         'mll_200_300':0,
         'mll_300_inf':0,          
         'mll_100_inf':111,
         'mll_130_inf':0,
         'mll_150_inf':0,
         'mll_200_inf':0}

cut = {'ptxetcone20':[-0.1,0.2],'pt':[] if isfake else [25],'eta1':[-2.47,2.47],'z0sintheta':[0.5],'d0sigd0':([3] if isuu else [5]),'dRlepljet':[0.4],'dRlepbjet':[0.2],'jetMV2c10':[0.18],
       'mll_100_150':([111,150,200,300] if issf else [111]),
       'mll_150_200':([111,150,200,300] if issf else [111]),
       'mll_200_300':([111,150,200,300] if issf else [111]),
       'mll_300_inf':[111], 
       'mll_100_inf':[111],
       'mll_130_inf':[300],
       'mll_150_inf':[111],
       'mll_200_inf':[111]
}




# Backgrounds
#if "gg" in channel:
#	backgrounds = ['Higgs'] 
#else:
doFake = False

backgrounds = ['Zjets', 'Top', 'Diboson', 'Wjets'] 

SUSYC1C1 = [392501,392502,392504,392506,392507,392509,392513,392517,392518,392521]
SUSYslsl = [392916,
            392918,
            392920,
            392924,
            392925,
            392936,
            392942,
            392951,
            392962,
            392964,
            392982,
            392985,
            392996,
            392999]


signals = SUSYslsl

Zjets = [364100, 364101, 364102, 364103, 364104, 364105, 364106, 364107, 364108, 364109, 364110, 364111, 364112, 364113, 364114, 364115, 364116, 364117, 364118, 364119, 364120, 364121, 364122, 364123, 364124, 
         364125, 364126, 364127, 364128, 364129, 364130, 364131, 364132, 364133, 364134, 364135, 364136, 364137, 364138, 364139, 364140, 364141]

Wjets = [364156, 364157, 364158, 364159, 364160, 364161, 364162, 364163, 364164, 364165, 364166, 364167, 364168, 364169, 364170, 364171, 364172, 364173, 364174, 364175, 364176, 364177, 364178, 364179, 364180, 
         364181, 364182, 364183, 364184, 364185, 364186, 364187, 364188, 364189, 364190, 364191, 364192, 364193, 364194, 364195, 364196, 364197]

Diboson = [361600, 361601, 361602, 361603, 361604, 361606, 361607, 361609, 361610] 

Top = [410000, 410011, 410012, 4100013, 410014, 410025, 410026]

Higgs = [341081, 343981, 345041, 345318, 345319]

#Higgs = [341122,341155,341947,341964,344235,345060,345323,345324,345325,345327,345336,345337,345445]


fileIDs_bkg = {'Diboson':Diboson, 'Zjets':Zjets, 'Wjets':Wjets, 'Top':Top, 'Higgs':Higgs}

#fileIDs_sig = SUSYC1C1#
sigdic = getSigDic()

hist_bkg = {}
hist_sig = {}
for ch in channel:
        hist_bkg[ch] = {}
        hist_sig[ch] = {}        
        for var in variables:
                if not plotvar == "all" and not var in plotvar: continue
                hist_bkg[ch][var] = {}
                hist_sig[ch][var] = {}
                for bkg in backgrounds:
                        hist_bkg[ch][var][bkg] = TH1F() 
                for sig in signals:
                        hist_sig[ch][var][sig] = TH1F() 


nsig = 0
for idi in signals:
	colours[idi] = kSpring+nsig
	nsig += 1


# Extract info about cross section and sum of weights from infofile 

info = {} 
for key in infos.keys(): 
        ID = infos[key]['DSID']
        info[ID] = {} 
        info[ID]['xsec'] = infos[key]['xsec'] 
        info[ID]['sumw'] = infos[key]['sumw'] 
        info[ID]['events'] = infos[key]['events']


# Function for making histograms

L = 10.6 # integrated luminosity = 10.6 fb^-1 
    
def fill_hist(h, h_name, key, ID, var, doAdd = True):

        #print h_name
	h_midl = infile.Get(h_name).Clone("h_midl")

	xsec = 1000*info[ID]['xsec']
	nev = info[ID]['sumw'] 
	
	N_mc = xsec*L

	sf = N_mc/nev  

	if not h.GetName() or not doAdd: 
		h=infile.Get(h_name)  
		h.Scale(sf)
		n = h.GetNbinsX()
		for i in range(n):
                        bc = h.GetBinContent(i)
			if bc < 0: 
				h.SetBinContent(i,0)
		h.SetFillColor(colours[key])
		h.SetLineColor(colours[key])
		h.SetDirectory(0)
		if var in rebin.keys():
			h = h.Rebin(rebin[var])
	else:
		h_midl.Scale(sf)
		if var in rebin.keys():
			h_midl = h_midl.Rebin(rebin[var])
		n = h_midl.GetNbinsX()
		for i in range(n):
			bc = h_midl.GetBinContent(i)
		        if bc < 0: 
				h_midl.SetBinContent(i,0) 
		h.Add(h_midl)


	#print var, h.GetBinWidth(1)
	return h  


# Loop over files in MC directory  

for ch in channel:
        print ch
        for filename in os.listdir('%s/MC/'%indir):
                if '.root' in filename: 
                        filepath = indir+'/MC/'+filename 
                        infile = TFile(filepath)
                        file_id = int(filename.split('.')[2])
                        #print infile
                        for var in variables:
                                #print ch, var
                                if "d0z0" in ch and not var in ['d0sigd0','z0sintheta','etcone20','ptxetcone20']: continue
                                if not plotvar == "all" and not var in plotvar: continue
                                for bkg in backgrounds:
                                        if file_id in fileIDs_bkg[bkg]: 
                                                #print file_id, bkg, 'h_'+ch+'_'+var
                                                #print hist_bkg[var][bkg].GetBinWidth(1)
                                                hist_bkg[ch][var][bkg] = fill_hist(hist_bkg[ch][var][bkg], 'h_'+ch+'_'+var, bkg, file_id, var)
                                for sig in signals:
                                        if file_id in signals: 
                                                hist_sig[ch][var][sig] = fill_hist(hist_sig[ch][var][sig], 'h_'+ch+'_'+var, sig, sig, var, False)
                                                #print "var = %s, sig = %s, name = %s, int = %.2f" %(var,sig,hist_sig[var][sig].GetName(),hist_sig[var][sig].Integral())


                                                
# Get data 	
data = TFile('%s/Data/hist.Data.2016.root'%indir)
hist_d ={}

if doFake:
    if os.path.isfile('%s/Fakes/hist.Fakes.2016.root'%indir):
        fakes = TFile('%s/Fakes/hist.Fakes.2016.root'%indir)
        backgrounds.append("Fakes")
    else:
        print "WARNING \t Could not find fake file"
        doFake = False
    

for ch in channel:
        hist_d[ch] = {}
        for var in variables:
                if not plotvar == "all" and not var in plotvar: continue
                if "d0z0" in ch and not var in ['d0sigd0','z0sintheta','etcone20','ptxetcone20']: continue
                if doFake:
                    hist_bkg[ch][var]['Fakes'] = fakes.Get('h_'+ch+'_'+var)
                    hist_bkg[ch][var]['Fakes'].SetFillColor(kPink)
		    hist_bkg[ch][var]['Fakes'].SetLineColor(kPink)
                    if var in rebin.keys():
                        hist_bkg[ch][var]['Fakes'] = hist_bkg[ch][var]['Fakes'].Rebin(rebin[var])
		    hist_bkg[ch][var]['Fakes'].SetDirectory(0)
                hist_d[ch][var] = data.Get('h_'+ch+'_'+var) 
                hist_d[ch][var].SetMarkerStyle(20)
                hist_d[ch][var].SetMarkerSize(0.7)
                hist_d[ch][var].SetLineColor(kBlack)
                hist_d[ch][var].GetYaxis().SetTitle("Events / %.2f"%hist_d[ch][var].GetBinWidth(1))
                hist_d[ch][var].GetXaxis().SetTitle(xtitles[var]) 
                hist_d[ch][var].GetXaxis().SetTitleFont(43)
                hist_d[ch][var].GetXaxis().SetTitleSize(16)
                hist_d[ch][var].GetYaxis().SetTitleFont(43)
                hist_d[ch][var].GetYaxis().SetTitleSize(16)
                hist_d[ch][var].GetXaxis().SetLabelFont(43)
                hist_d[ch][var].GetXaxis().SetLabelSize(16)
                hist_d[ch][var].GetYaxis().SetLabelFont(43)
                hist_d[ch][var].GetYaxis().SetLabelSize(16)
                hist_d[ch][var].GetXaxis().SetTitleOffset(4)
                hist_d[ch][var].GetYaxis().SetTitleOffset(1.5)
                if var in rebin.keys():
                        hist_d[ch][var] = hist_d[ch][var].Rebin(rebin[var])

looseName = ""
tightName = ""
for key in hist_d.keys():
        if "LfakeCR" in key or "LrealCR" in key:
                looseName = key
        if "TfakeCR" in key or "TrealCR" in key:
                tightName = key


if looseName and tightName:
        print "Loose = ",looseName
        print "Tight = ",tightName
        effName = looseName.replace("_L","_eff")
        hist_d[effName] = {}
        hist_bkg[effName] = {}
        if "real" in looseName: arr = array.array('f',[25,30,35,40,45,50,60,70,80,90,100,120,150,200,1500])
        if "fake" in looseName: arr = array.array('f',[25,35,50,80,1500])
        for var in hist_d[tightName].keys():
                if "pt" in var:
                        hist_d[tightName][var] = doRebin(hist_d[tightName][var],arr,True)
                        hist_d[looseName][var] = doRebin(hist_d[looseName][var],arr,True)
                hist_d[effName][var] = hist_d[tightName][var].Clone(effName)
                print "Loose = ",hist_d[looseName][var].Integral()
                print "Tight = ",hist_d[tightName][var].Integral()
                print "Efff  = ",hist_d[effName][var].Integral()
                hist_d[effName][var].Divide(hist_d[tightName][var],hist_d[looseName][var])
                setRatioErrors(hist_d[tightName][var],hist_d[looseName][var],hist_d[effName][var])
        for var in hist_bkg[tightName].keys():
                hist_bkg[effName][var] = {}
                for bkg in hist_bkg[tightName][var].keys():
                        if 'pt' in var:
                                hist_bkg[tightName][var][bkg] = doRebin(hist_bkg[tightName][var][bkg],arr,False,bkg)
                                hist_bkg[looseName][var][bkg] = doRebin(hist_bkg[looseName][var][bkg],arr,False,bkg)
                        hist_bkg[effName][var][bkg] = hist_bkg[tightName][var][bkg].Clone(effName)
                        hist_bkg[effName][var][bkg].Divide(hist_bkg[looseName][var][bkg])
                        setRatioErrors(hist_bkg[tightName][var][bkg],hist_bkg[looseName][var][bkg],hist_bkg[effName][var][bkg])

# Style histograms, make stack and histograms with full background

stack = {} 
hist_r = {}
hist_mc = {}
for ch in channel:
        stack[ch] = {} 
        hist_r[ch] = {}
        hist_mc[ch] = {}
        for var in variables:
                if not plotvar == "all" and not var in plotvar: continue
                if "d0z0" in ch and not var in ['d0sigd0','z0sintheta','etcone20','ptxetcone20']: continue
                stack[ch][var] = THStack(var, "")  
                hist_mc[ch][var] = TH1F()
                hist_r[ch][var] = TH1F()
                for bkg in reversed(backgrounds): 
                        hist_bkg[ch][var][bkg].GetYaxis().SetTitle("Events / %.2f"%hist_d[ch][var].GetBinWidth(1))
                        hist_bkg[ch][var][bkg].GetXaxis().SetTitle(xtitles[var]) 
                        hist_bkg[ch][var][bkg].GetXaxis().SetTitleFont(43)
                        hist_bkg[ch][var][bkg].GetXaxis().SetTitleSize(16)
                        hist_bkg[ch][var][bkg].GetYaxis().SetTitleFont(43)
                        hist_bkg[ch][var][bkg].GetYaxis().SetTitleSize(16)
                        hist_bkg[ch][var][bkg].GetXaxis().SetLabelFont(43)
                        hist_bkg[ch][var][bkg].GetXaxis().SetLabelSize(16)
                        hist_bkg[ch][var][bkg].GetYaxis().SetLabelFont(43)
                        hist_bkg[ch][var][bkg].GetYaxis().SetLabelSize(16)
                        hist_bkg[ch][var][bkg].GetXaxis().SetTitleOffset(4)
                        hist_bkg[ch][var][bkg].GetYaxis().SetTitleOffset(1.5)
                        stack[ch][var].Add(hist_bkg[ch][var][bkg]) 
                        if not hist_mc[ch][var].GetName(): 
                                hist_mc[ch][var] = hist_bkg[ch][var][bkg].Clone()
                        else: 
                                hist_mc[ch][var].Add(hist_bkg[ch][var][bkg])
                        hist_r[ch][var] = hist_d[ch][var].Clone()
                        hist_r[ch][var].Divide(hist_mc[ch][var])
                        hist_r[ch][var].SetTitle("")
                        hist_r[ch][var].GetXaxis().SetTitle(xtitles[var])
                        hist_r[ch][var].GetYaxis().SetTitle("Data/#SigmaMC")
                        hist_r[ch][var].GetYaxis().SetNdivisions(506)
                        hist_r[ch][var].SetMarkerStyle(20)
                        hist_r[ch][var].SetMarkerSize(0.7)
                        if var in xmax.keys() and var in xmin.keys():
                                hist_r[ch][var].GetXaxis().SetRangeUser(xmin[var],xmax[var])

# Make plot legend 

leg = TLegend(0.50,0.45,0.88,0.88)
leg.SetFillStyle(4000)  
leg.SetFillColor(0)
leg.SetTextFont(42)
leg.SetTextSize(0.05)
leg.SetBorderSize(0)

bkg_labels = {'Zjets':'Z+jets', 'Top':'Top', 'Diboson':'Diboson', 'Wjets':'W+jets', 'Higgs':'Higgs', 'Fakes':'FNP'}

sig_labels = {}
for s in signals:
	descr = sigdic[str(s)] 
	if "C1C1_SlepSnu" in descr:
		m1 = descr.split("_")[5].replace("p0","")
		m2 = descr.split("_")[6].replace("p0","")
		sig_labels[s] = "m(#chi_{1}^{#pm},#chi_{1}^{0}) = (%s,%s) GeV"%(m1,m2)
        if "SlepSlep_direct" in descr:
		m1 = descr.split("_")[4].replace("p0","")
		m2 = descr.split("_")[5].replace("p0","")

                m1 = m1.replace("p5","")
		m2 = m2.replace("p5","")
		sig_labels[s] = "m(#tilde{l},#chi_{1}^{0}) = (%s,%s) GeV"%(m1,m2)


key0 = hist_bkg.keys()[0]
key1 = hist_bkg[key0].keys()[0]
for bkg in backgrounds: 
        leg.AddEntry(hist_bkg[key0][key1][bkg], bkg_labels[bkg], "f") 
leg.AddEntry(hist_d[key0][key1],"Data","ple")

key0 = hist_sig.keys()[0]
key1 = hist_sig[key0].keys()[0]
for sig in hist_sig[key0][key1].keys():
	if "C1C1_SlepSnu" in sigdic[str(sig)] and not sig in [392518,392513,392506]: continue
        if "SlepSlep_direct" in sigdic[str(sig)] and not sig in [392924,392982]: continue
	leg.AddEntry(hist_sig[key0][key1][sig],sig_labels[sig], "l") 


MMfiles = []
for ch in channel:
        selection = ""
        if "ee" in ch: 
                selection = "ee" 
        if "uu" in ch: 
                selection = "#mu#mu"
        if "gg" in ch: 
                selection = "#gamma#gamma"
        if "sf" in ch: 
            selection = "sf"
        if "eu" in ch: 
            selection = "df"

        if "os" in ch: 
                selection += "os"
        if "ss" in ch: 
                selection += "ss"

        if "_signal" in ch: 
                selection += " - signal"

        if "yes" in ch: 
                selection += " trig"
        if "nod0z0" in ch:
                if "ee" in ch: selection = " e^{#pm} no d_{0}, z_{0} cut"
                if "uu" in ch: selection = " #mu^{#pm} no d_{0}, z_{0} cut"
        elif "no" in ch: 
                selection += " no trig"

        if "fakeCR" in ch:
            selection = "#splitline{"+selection
            selection += ", E_{T}^{miss} < 40 GeV,}{ N(b-jets) == 1}"
        if "realCR" in ch:
            selection += ", |m_{ll}-m_{Z}|<15 GeV"

        # Make plots 
        madeFiles = []
        for var in variables: 
                if not plotvar == "all" and not var in plotvar: continue
                if "d0z0" in ch and not var in ['d0sigd0','z0sintheta','etcone20','ptxetcone20']: continue

                addselection = ""
                if "mll_" in var:
                    mt2low = var.split("_")[1]
                    mt2high = var.split("_")[2]
                    if not mt2high == "inf":
                        addselection = "%s<m_{T2}<%s" %(mt2low,mt2high)
                    else:
                        addselection = "m_{T2}>%s" %mt2low
                cnv = TCanvas("cnv_"+var,"", 500, 500)
                cnv.SetTicks(1,1) 
                cnv.SetLeftMargin(0.13) 
                #cnv.SetLogy()

                p1 = TPad("p1", "", 0, 0.35, 1, 1) 
                p2 = TPad("p2", "", 0, 0.0, 1, 0.35) 


                p1.SetBottomMargin(0.0) 
                p1.Draw() 
                p1.cd()
                if "CR" in ch:
                    p1.SetLogx()

                maximum = max((stack[ch][var].GetStack().Last()).GetBinContent((stack[ch][var].GetStack().Last()).GetMaximumBin()),hist_d[ch][var].GetBinContent(hist_d[ch][var].GetMaximumBin()))

                if maximum > 0: p1.SetLogy()

                stack[ch][var].Draw("hist")
                stack[ch][var].SetMinimum(10E-2)
                stack[ch][var].GetYaxis().SetTitle("Events / %.2f"%hist_d[ch][var].GetBinWidth(1))
                stack[ch][var].GetYaxis().SetTitleFont(43)
                stack[ch][var].GetYaxis().SetTitleSize(16)
                stack[ch][var].GetYaxis().SetLabelFont(43)
                stack[ch][var].GetYaxis().SetLabelSize(16)
                stack[ch][var].GetYaxis().SetTitleOffset(1.5)
                if var in xmax.keys() and var in xmin.keys():
                        stack[ch][var].GetXaxis().SetRangeUser(xmin[var],xmax[var])
                if var in ['eta1', 'eta2', 'phi1', 'phi2', 'trigDec', 'z0'] or "nlep" in var: 
                        maximum = stack[ch][var].GetMaximum() 
                        stack[ch][var].SetMaximum(maximum*10E6)
                        ymax = maximum*10E6
                elif var in ['mll', 'met', 'z0sintheta', 'd0sigd0', 'jetpt', 'mt2']: 
                        stack[ch][var].SetMaximum(maximum*1.5)
                        ymax = maximum*1.5
                elif var in ['mll_100_inf']:
                    stack[ch][var].SetMaximum(10E4)
                else:
                        stack[ch][var].SetMaximum(maximum*10E4)
                        ymax = maximum*10E4

                nsig = 0
                for sig in hist_sig[ch][var].keys():
                        #print "Drawing ",sig
                        if "C1C1_SlepSnu" in sigdic[str(sig)] and not sig in [392518,392513,392506]: continue
                        hist_sig[ch][var][sig].SetLineStyle(nsig)
                        hist_sig[ch][var][sig].SetLineColor(kSpring+nsig)
                        hist_sig[ch][var][sig].SetLineWidth(2)
                        hist_sig[ch][var][sig].SetFillStyle(0)
                        hist_sig[ch][var][sig].Draw("hist same")
                        nsig += 1
                        if nsig > 3: break


                hist_d[ch][var].Draw("same e0")
                leg.Draw("same") 

                s = TLatex()
                s.SetNDC(1);
                s.SetTextAlign(13);
                s.SetTextColor(kBlack);
                s.SetTextSize(0.044);
                s.DrawLatex(0.2,0.86,"#font[72]{ATLAS} Open Data");
                s.DrawLatex(0.2,0.81,"#bf{#sqrt{s} = 13 TeV,^{}%.1f^{}fb^{-1}}" % (L));
                s.DrawLatex(0.2,0.76,"#bf{"+selection+" "+addselection+"}");
                

                #print type(hist_r) 
                ymax = stack[ch][var].GetMaximum()*1.2
                ymin = stack[ch][var].GetMinimum()
                xmaxi = hist_d[ch][var].GetNbinsX()#hist_d[ch][var].GetBinLowEdge(hist_d[ch][var].GetNbinsX()+1)
                print xmaxi
                #print ymin,ymax
                cutline = []
                ar1 = []
                if var in cut.keys():
                    print var
                    nc = 0
                    for c in cut[var]:
                        print c
                	cutline.append(TLine(cut[var][nc],ymin,cut[var][nc],ymax*4))
                	cutline[-1].SetLineColor(kRed)
                        cutline[-1].SetLineWidth(2)
                        if nc%2 == 0:
                            print (1+((xmaxi/cut[var][nc])/(xmaxi*hist_d[ch][var].GetBinWidth(1)*30)))
                            ar1.append(TArrow(cut[var][nc],ymax*4,cut[var][nc]+hist_d[ch][var].GetBinWidth(1),ymax*4))
                        else:
                            ar1.append(TArrow(cut[var][nc],ymax*4,cut[var][nc]-hist_d[ch][var].GetBinWidth(1),ymax*4))
                        ar1[-1].SetLineColor(kRed)
                        ar1[-1].SetLineWidth(2)
                        ar1[-1].SetArrowSize(0.01)
                        if not 'mll_' in var: ar1[-1].Draw();
                        cutline[-1].Draw()
                        nc += 1
                #print type(hist_r) 
                p1.Update() 

                p1.RedrawAxis() 

                cnv.cd() 

                p2.Draw() 
                p2.cd() 

                p2.SetGridy()

                if "CR" in ch:
                    hist_r[ch][var].GetXaxis().SetTitleFont(43)
                    hist_r[ch][var].GetXaxis().SetTitleSize(16)
                    hist_r[ch][var].GetYaxis().SetTitleFont(43)
                    hist_r[ch][var].GetYaxis().SetTitleSize(16)
                    hist_r[ch][var].GetXaxis().SetLabelFont(43)
                    hist_r[ch][var].GetXaxis().SetLabelSize(16)
                    hist_r[ch][var].GetYaxis().SetLabelFont(43)
                    hist_r[ch][var].GetYaxis().SetLabelSize(16)
                    hist_r[ch][var].GetXaxis().SetTitleOffset(4)
                    hist_r[ch][var].GetYaxis().SetTitleOffset(1.5)
                    p2.SetLogx()    
                
                hist_r[ch][var].SetMaximum(1.99) 
                hist_r[ch][var].SetMinimum(0.01) 	
                hist_r[ch][var].Draw("0PZ") 

                p2.SetTopMargin(0) 
                p2.SetBottomMargin(0.35) 
                p2.Update()        
                p2.RedrawAxis()

                cnv.cd() 
                cnv.Update() 

                if not os.path.exists('Plots'+indir):
                        os.makedirs('Plots'+indir)
                cnv.Print('Plots'+indir+'/'+ch+'_'+var+'.png') 
                if "gg" in ch and 'mll' in var:
                        tfile = TFile('Plots'+indir+'/'+ch+'_'+var+'.root',"RECREATE")
                        (stack[ch][var].GetStack().Last()).Write("signal")
                        hist_d[ch][var].Write("data")
                        tfile.Close()
                if maximum > 0 and not "CR" in ch:
                    madeFiles.append('Plots'+indir+'/'+ch+'_'+var+'.png')
                elif maximum > 0 and "CR" in ch:
                    MMfiles.append('Plots'+indir+'/'+ch+'_'+var+'.png')

                cnv.Close() 

        string = makeBeamerSlides(madeFiles)
        outfile = open("tex/"+ch+'.tex','w')
        outfile.write(string)
        outfile.close()


if looseName:
    ROOT.gROOT.SetBatch(0)
    c = TCanvas("c_"+effName,"", 500, 500)
    c.SetLogx()
    leg1 = TLegend(0.498567,0.2173,0.664756,0.398734)
    leg1.SetFillStyle(4000)  
    leg1.SetFillColor(0)
    leg1.SetTextFont(42)
    leg1.SetTextSize(0.03)
    leg1.SetBorderSize(0)
    hist_d[effName]['pt'].SetTitle("")
    hist_d[effName]['pt'].Draw("e1")
    hist_d[effName]['pt'].GetYaxis().SetRangeUser((0.8 if "real" in effName else 0.5),1)
    hist_d[effName]['pt'].GetYaxis().SetTitle("Real Efficiency" if "real" in effName else "Fake Rate")
    hist_d[effName]['pt'].GetXaxis().SetTitle("lepton p_{T} [GeV]")
    MMtfile = TFile("MM_fake_real.root","UPDATE")
    hist_d[effName]['pt'].Write()
    MMtfile.Close()
    leg1.AddEntry(hist_d[effName]['pt'],"Data","lp")
    for bkg in hist_bkg[effName]['pt'].keys():
        hist_bkg[effName]['pt'][bkg].SetMarkerColor(colours[bkg])
        hist_bkg[effName]['pt'][bkg].SetMarkerStyle(20)
        hist_bkg[effName]['pt'][bkg].SetMarkerSize(0.7)
        hist_bkg[effName]['pt'][bkg].SetLineColor(colours[bkg])
        hist_bkg[effName]['pt'][bkg].Draw("e1 same")
        leg1.AddEntry(hist_bkg[effName]['pt'][bkg],bkg,"lp")
    leg1.Draw()
    c.Print('Plots'+indir+'/'+effName+'_pt.png')
    MMfiles.append('Plots'+indir+'/'+effName+'_pt.png')
    string = makeBeamerSlides(MMfiles)
    outfile = open("tex/"+effName+'.tex','w')
    outfile.write(string)
    outfile.close()

