# openDataAna

First one needs to specify the location of the openData files in *runSelector.py*. I.e. these line:

```
if arg1 == 'Data': 
        input_dir = '<path-to-data>'
elif arg1 == 'Fakes': 
        input_dir = '<path-to-fake-estimation-file>'
elif arg1 == 'MC': 
        input_dir = '<path-to-SM-signal-samples>'
        input_dir1 = '<path-to-SM-background-samples>'
```

Then run the script by typing one of the following (depending on what you'd like to run over):


```
python runSelector.py Data`
python runSelector.py Fakes
python runSelector.py MC

```

The script produces files with histograms in the directory *Histograms*

Plotting can be done with the *MakePlots.py* where you specify the channel and variable to plot (see code for details) in the input arguments. Choose *all* as variable to plot all available variables. E.g

```
python MakePlots.py eeos_yes all 
```
If you want to plot all variables in the oposite sign di-electron channel. Look at the histograms to see what else can be plotted. The plots are saved in a directory names *PlotsHistograms*